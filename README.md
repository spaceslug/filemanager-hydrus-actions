# filemanager-hydrus-actions

Right click context menu actions adding files/directories to Hydrus for Dolphin file manager and Nemo file manager. \
Should work for both remote and local Hydrus intances.

## What it does

Right click context menu actions for file manager which make it easy to quickly add files to Hydrus.Sends the selected files/directories/archives to Hydrus, with user defined tags. It works recursively (files in subdirectories will also be found, etc).
By default it also extracts archives and sends their contents instead of the archive files themselves. It does this recursively too.
It does not delete any files and there is a confirmation dialog with a list of files before the files are sent to Hydrus.

## Requirements

Dolphin file manager or Nemo file manager, kdialog, 7zip (for extracting .rar and .7z files), notify-send, Hydrus API module by cryzed (https://gitlab.com/cryzed/hydrus-api).

## How to install 

For Dolphin copy the .desktop files to `$HOME/.local/share/kservices5/ServiceMenus/`. \
For Nemo copy the .nemo_action files to `$HOME/.local/share/nemo/actions/`. \
Copy add-to-hydrus.cfg to `$HOME/.config/` and set your API key and Hydrus url. \
Make sure `add-to-hydrus` is executable and it is in the search path. For example `$HOME/.local/bin/`. \
To check if it works, call it first from the command line without any arguments. After hitting OK on the tag dialog, you should get a 'No files to add' message.

## Credits
Originaly created by prkc (https://gitgud.io/prkc/dolphin-hydrus-actions) 

## License

GPLv3
